package client;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import logger.LogSetup;

import common.messages.KVMessage;
import common.messages.KVMessage.StatusType;
import common.messages.TextMessage;
import common.messages.TextMessage.MessageParameters;



public class KVStore implements KVCommInterface {

	private Logger logger = Logger.getRootLogger();
	
	// Connection parameters
	private String serverAddress;
	private int serverPort;
	private boolean running;
	public enum SocketStatus{CONNECTED, DISCONNECTED, CONNECTION_LOST};
	private Socket clientSocket;
	private OutputStream output;
 	private InputStream input;
	

 	// Receiver buffer/message parameters:
	private static final int BUFFER_SIZE = 1024;
	private static final int DROP_SIZE = 1024 * BUFFER_SIZE;
	public static final byte HEADER_OPEN = 0x7E;
	public static final byte HEADER_CLOSE = 0x5E;
	public static final byte MESSAGE_STUFFING = 0x23;

	
	
 	
 	
 	
	/**
	 * Initialize KVStore with address and port of KVServer
	 * 
	 * @param address the address of the KVServer
	 * @param port the port of the KVServer
	 */
	public KVStore(String address, int port) {
		this.serverAddress = address;
		this.serverPort = port;
	}
	
	/**
	 * Connects client to the server in order to send and receive messages
	 */
	public void connect() throws Exception {
		clientSocket = new Socket(this.serverAddress, this.serverPort);
		
		try {
			output = clientSocket.getOutputStream();
			input = clientSocket.getInputStream();
			setRunning(true);
			TextMessage connect_response = receiveMessage();
			
			if(connect_response != null){
				// Client did not fail to read response, ie server disconnect
				System.out.println(connect_response.getMsg()); // Server informs client they have established a connection
			}
			else{
				System.out.println("Server disconnected during connection phase.");
				setRunning(false);
			}
		} 
		
		catch (IOException ioe) {
			System.out.println("Input and output streams to server could not be established!");
			logger.error("Input and output stream could not be established!");
			System.exit(1);
		} 
	}


	/**
	 *  Disconnect client communication from the server and cleans up resources used.
	 */
	public void disconnect() {
		
		try{
			setRunning(false);
			logger.info("tearing down the connection ...");
			if (clientSocket != null) {
				input.close();
				output.close();
				clientSocket.close();
				clientSocket = null;
				logger.info("connection closed!");
			}
		}
		catch (IOException ioe) {
			logger.error("Unable to close connection!");
		}
	}

	/**
	 * 
	 * Attempts to insert a <key,value> tuple into the server database.
	 * 
  	 * @param key: key associated with tuple to insert in server table
	 * @param value: value associated with tuple to insert in server table
	 */
	public KVMessage put(String key, String value) throws Exception {

		TextMessage request = new TextMessage(null, key, value ,StatusType.PUT);		
		sendMessage(request);
		TextMessage response = receiveMessage();
		
		// Ie. server disconnect:
		if(response == null){
			setRunning(false);
		}
		
		return response;
	}

	/**
	 * 
	 * Attempts to retrieve a <key,value> tuple from the server database using a key.
	 * 
  	 * @param key: key associated with tuple to retrieve from server
	 */
	public KVMessage get(String key) throws Exception {
		
		TextMessage request = new TextMessage(null, key, null ,StatusType.GET);
		sendMessage(request);
		TextMessage response = receiveMessage();
		
		// Ie. server disconnect:
		if(response == null){
			setRunning(false);
		}
		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * HELPER FUNCTIONS
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	
	public synchronized void closeConnection() {
		logger.info("try to close connection ...");
		
		try {
			tearDownConnection();
			handle_status(SocketStatus.DISCONNECTED);
		} catch (IOException ioe) {
			logger.error("Unable to close connection!");
		}
	}
	
	private void tearDownConnection() throws IOException {
		setRunning(false);
		logger.info("tearing down the connection ...");
		if (clientSocket != null) {
			//input.close();
			//output.close();
			clientSocket.close();
			clientSocket = null;
			logger.info("connection closed!");
		}
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public void setRunning(boolean run) {
		running = run;
	}
	
	
	/**
	 * Method sends a TextMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	public void sendMessage(TextMessage msg) throws IOException {
		byte[] msgBytes = msg.getMsgBytes(); 
		String string_format = new String(msgBytes);
		output.write(msgBytes, 0, msgBytes.length);
		output.flush();
		logger.info("Send message:\t '" + string_format + "'");
    }
	
	
	
	
	
	
	
	private TextMessage receiveMessage() throws IOException {
		

		// Message to return is initially null to indicate failure reading bytes:
		TextMessage msg = null;
		
		
		int index = 0;
		byte[] msgBytes = null, tmp = null;
		byte[] bufferBytes = new byte[BUFFER_SIZE];
		byte cur_byte = 0x0;
		byte prev = 0x0;
		
		/* read first char from stream */
		boolean reading = true;


		// While we haven't read the opening header for a message, keep reading before we record anything
		//
		while(!(cur_byte == HEADER_OPEN && prev != MESSAGE_STUFFING) && reading){
			prev = cur_byte;
			cur_byte = (byte)input.read();
			

			if(cur_byte == -1){
				reading = false;
				running = false;
			}
		}
		
		// Error while reading, ie server close:
		if(!running)
			return msg;
		
		
		
		/* read next char from stream */
		cur_byte = (byte) input.read();
		
				
				
		// While we haven't read the closing header for the message, record everything, this is the JSON object
		//
		boolean stuff_last_char = false;
		
		while(!(cur_byte == HEADER_CLOSE &&  prev!= MESSAGE_STUFFING) && reading) {
			
			
			// IF we enter and we previously skipped writing because of a stuff
			// The stuff is an actual character within the message:
			//
			if(stuff_last_char){

				// This was not an attempt to stuff a header open either:
				//
				if(cur_byte != HEADER_OPEN){
					bufferBytes[index] = cur_byte;
					prev = cur_byte;
							
					index++;
				}
			}
			
			
			
			// Update whether the current character is a stuff character
			if(cur_byte == MESSAGE_STUFFING)
				stuff_last_char = true;
			else
				stuff_last_char = false;

				
			
			
			if(!stuff_last_char){
					
				/* CR, LF, error */
				/* if buffer filled, copy to msg array */
				if(index == BUFFER_SIZE) {
					if(msgBytes == null){
						tmp = new byte[BUFFER_SIZE];
						System.arraycopy(bufferBytes, 0, tmp, 0, BUFFER_SIZE);
					} else {
						tmp = new byte[msgBytes.length + BUFFER_SIZE];
						System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
						System.arraycopy(bufferBytes, 0, tmp, msgBytes.length,
								BUFFER_SIZE);
					}
	
					msgBytes = tmp;
					bufferBytes = new byte[BUFFER_SIZE];
					index = 0;
				} 
				
				/* only read valid characters, i.e. letters and constants */
				bufferBytes[index] = cur_byte;						
				index++;
				
				/* stop reading is DROP_SIZE is reached */
				if(msgBytes != null && msgBytes.length + index >= DROP_SIZE) {
					reading = false;
				}
			
			}
			
			prev = cur_byte;
			/* read next char from stream */
			cur_byte = (byte) input.read();
			
			if(cur_byte == -1){
				reading = false;
				running = false;
			}
				
		}
		
		// Error reading bytes, ie server close:
		if(!running)
			return msg;
		
		
		
		if(msgBytes == null){
			tmp = new byte[index];
			System.arraycopy(bufferBytes, 0, tmp, 0, index);
		} else {
			tmp = new byte[msgBytes.length + index];
			System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
			System.arraycopy(bufferBytes, 0, tmp, msgBytes.length, index);
		}
		
		
		// MsgBytes contains actual message which is a JSON string
		//
		// We convert the JSON string into a MessageParameters object for convenience
		//
		msgBytes = tmp;
		String string_format = new String(msgBytes);
		
		Gson gson = new Gson();
		MessageParameters json_parameters = gson.fromJson(string_format, MessageParameters.class);
		
		
		/* build final String */
		msg = new TextMessage(json_parameters.get_msg(), json_parameters.get_key(), json_parameters.get_value(), json_parameters.get_status());
		
		logger.info("RECEIVE \t<" 
				+ clientSocket.getInetAddress().getHostAddress() + ":" 
				+ clientSocket.getPort() + ">: '" 
				+ string_format + "'");
				
		return msg;		

    }
	
	
	public void handle_status(SocketStatus status) {
		if(status == SocketStatus.CONNECTED) {

		} else if (status == SocketStatus.DISCONNECTED) {
			//System.out.print(PROMPT);
			System.out.println("Connection terminated: " 
					+ serverAddress + " / " + serverPort);
			
		} else if (status == SocketStatus.CONNECTION_LOST) {
			System.out.println("Connection lost: " 
					+ serverAddress + " / " + serverPort);
			//System.out.print(PROMPT);
		}
		
	}
	 
}