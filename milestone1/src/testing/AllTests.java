package testing;

import java.io.IOException;

import org.apache.log4j.Level;

import app_kvServer.KVServer;
import junit.framework.Test;
import junit.framework.TestSuite;
import logger.LogSetup;

public class AllTests {

	private static KVServer test_server;
		
	public static Test suite() {
		TestSuite clientSuite = new TestSuite("Basic Storage ServerTest-Suite");
		clientSuite.addTestSuite(ConnectionTest.class);
		clientSuite.addTestSuite(InteractionTest.class); 
		clientSuite.addTestSuite(AdditionalTest.class); 


	
		// Non-performance testing setup
		try {		
			test_server = new KVServer(50001, 10, "FIFO");
			test_server.start();
			new LogSetup("logs/testing/test.log", Level.ERROR);
			System.out.println("Set up logger and server. \n");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		
		
		return clientSuite;
	}

}
