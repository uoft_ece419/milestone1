package testing;

import java.io.IOException;

import org.apache.log4j.Level;

import logger.LogSetup;

public class PerformanceMain{

    public static void main(String[] args) {

		System.out.println("Starting tests.....");

		try {		
			new LogSetup("logs/testing/server_test.log", Level.ERROR);
			System.out.println("Set up logger.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		// Create the performance tester:
		PerformanceTest speed_tests = new PerformanceTest();
		
		
		
		
		// TEST: Unique pairs == cache elements
		//
		// 4 clients , 20 cache elements, 20 unique pairs, 1000 requests per user, LFU/FIFO/LRU, port 50005
		//
		speed_tests.performance_test(4, 20, 20, 1000, "LFU", 50009);
		speed_tests.performance_test(4, 20, 20, 1000, "LRU", 50007);
		speed_tests.performance_test(4, 20, 20, 1000, "FIFO", 50008);
		
		// TEST: Unique pairs == 2 x cache elements
		//
		//speed_tests.performance_test(4, 20, 40, 100, "LRU", 50010);
		speed_tests.performance_test(4, 20, 40, 1000, "LFU", 50012);
		speed_tests.performance_test(4, 20, 40, 1000, "FIFO", 50011);
		
		// TEST: Unique pairs == 3 x cache elements
		//
		speed_tests.performance_test(4, 20, 60, 1000, "LRU", 50012);
		speed_tests.performance_test(4, 20, 60, 1000, "FIFO", 50013);
		speed_tests.performance_test(4, 20, 60, 1000, "LFU", 50014);


		System.out.println("Performance tests complete.\n\n\n\n");
		System.exit(0);
	}

}

