package testing;

import org.junit.Test;

import common.messages.KVMessage;
import common.messages.KVMessage.StatusType;

import client.KVStore;

import junit.framework.TestCase;

public class AdditionalTest extends TestCase {
	
	// TODO add your test cases, at least 10
	
	private KVStore kvClient;
	private KVStore kvClient2;
	
	
	/**
	* this is run before all tests
	* connect the 2 clients to server
	*/
	public void setUp() {
		kvClient = new KVStore("localhost", 50001);
		kvClient2 = new KVStore("localhost", 50001);
		try {
			kvClient.connect();
			kvClient2.connect();
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}

	/**
	* this is run after all tests
	* disconnect clients
	*/
	public void tearDown() {
		kvClient.disconnect();
		kvClient2.disconnect();
	}

	
	
	/**
	* reference: http://stackoverflow.com/questions/7888004/how-do-i-print-escape-characters-in-java
	* convert a string with special characters but without escape characters to one with escape characters
	*/
	public static String unEscapeString(String s){
		
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<s.length(); i++)
			switch (s.charAt(i)){
				case '\n': sb.append("\\n"); break;
				case '\t': sb.append("\\t"); break;
				// ... rest of escape characters
				default: sb.append(s.charAt(i));
			}
		return sb.toString();
	
	}	
	
	/**
	* test1
	* when there are spaces at the end of the value string
	* the value get should also contain all the spaces
	*/
	@Test
	public void testClientInputSpaceAsValue1(){
		
		String key = "foo_space";
		String value = "bar space  2space";
		KVMessage response = null;
		KVMessage response2 = null;
		Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		try {
			response2 = kvClient.get(key);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		//System.out.println("test1: value got is <" + response.getValue() + ">");

		assertTrue(ex == null && response.getStatus() == StatusType.PUT_SUCCESS && response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
	}
	
	/**
	* test2
	* when there are spaces at the beginning of the value string
	* the value get should also contain spaces
	*/
	@Test
	public void testClientInputSpaceAsValue2(){
		
		String key = "foo_space";
		String value = "  bar space  2space";
		KVMessage response = null;
		KVMessage response2 = null;
		Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		try {
			response2 = kvClient.get(key);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		//System.out.println("test2: value got is <" + response.getValue() + ">");

		assertTrue(ex == null && response.getStatus() == StatusType.PUT_SUCCESS && response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
	}
	
	/**
	* test3
	* special characters tests
	* input key value pair that contains \n \t etc
	*/
	@Test
	public void testClientInputSpecialChar(){
		
		String key = "foo_\nspecial";
		String value = "\\bar\tspecial";
		KVMessage response = null;
		KVMessage response2 = null;
		Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		try {
			response2 = kvClient.get(key);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		//System.out.println("test2: value got is <" + response.getValue() + ">");

		assertTrue(ex == null && response.getStatus() == StatusType.PUT_SUCCESS && response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
	}
	
	
	/**
	* test4
	* client1 put key-value pair and then client2 tries to get it
	* client2 should be able to get the key value pair client1 put
	*/
	@Test
	public void testMultipleClientsPutandGet(){
		
		String key = "multi_foo1";
		String value = "multi_bar1";
		KVMessage response = null;
		KVMessage response2 = null;
		Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		try {
			response2 = kvClient2.get(key);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		

		assertTrue(ex == null && response.getStatus() == StatusType.PUT_SUCCESS && response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
	}
	
	/**
	* test5
	* Multiple clients delete and get
	* one client deletes, the other one can't get
	*/
	@Test
	public void testMultiClientsDeleteAndGet(){
		
		String key = "multi_foo2";
		String value = "multi_bar2";
		KVMessage response = null;
		KVMessage response2 = null;
		KVMessage response3 = null;
		Exception ex = null;

		//client 1 put something
		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		//client 2 deletes it
		try {
			response2 = kvClient2.put(key, "null");
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		//client 1 tries to get it back
		try {
			response3 = kvClient.get(key);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		

		assertTrue(ex == null && response3.getStatus()==StatusType.GET_ERROR);
	}
	
	
		
	/**
	* test6
	* when cache overflows, the data get stored in the persistent storage
	* this way, we can test persistent storage
	*/
	@Test
	public void testCacheOverflow(){
		
		String[] keys = {"key0", "key1", "key2", "key3", "key4", "key5", "key6", "key7", "key8", "key9", "key10", "key11", "key12", "key13"};
		String[] values = {"value0", "value1", "value2", "value3", "value4", "value5", "value6", "value7", "value8", "value9", "value10", "value11", "value12", "value13"};
		
		KVMessage response = null;
		Exception ex = null;
		
		//client put 13 values in row
		
		for(int i=0; i<keys.length; i++){
			
			try {
				response = kvClient.put(keys[i], values[i]);
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}
			
		}
		
		
		//client gets all value
		
		for(int i=0; i<keys.length; i++){
			
			try {
				response = kvClient.get(keys[i]);
				assertTrue(response.getValue().equals(values[i]));//test point: values got back are the same as the input values
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}
			
		}
		
		
		assertTrue(ex==null);
		
		
	}
	
	/**
	* test7
	* cache overflows and the data gets stored in the persistent storage
	* this time, we store the data with spaces and special characters to make sure that it works
	*/
	@Test
	public void testCacheOverflowWithSpaces(){
		
		String[] keys = {"key0", "key1", "key2", "key3", "key4", "key5", "key6", "key7", "key8", "key9", "key10", "key11", "key12", "key13"};
		String[] values = {"value0\n", "\\value1", "value2", "value3", "value4", "value5", "value6", "value7", "value8", "value9", "value10", "value11", "value12", "value13"};
		
		KVMessage response = null;
		Exception ex = null;
		
		//client put 13 values in row
		
		for(int i=0; i<keys.length; i++){
			
			try {
				response = kvClient.put(unEscapeString(keys[i]), unEscapeString(values[i]));
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}
			
		}
		
		
		//client gets all value
		
		for(int i=0; i<keys.length; i++){
			
			try {
				response = kvClient.get(keys[i]);
				//System.out.println(response.getValue());
				//System.out.println(values[0]);
				assertTrue(response.getValue().equals(unEscapeString(values[i])));//test point: values got back are the same as the input values
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}
			
		}
		
		
		assertTrue(ex==null);
		
		
	}
	
	/**
	* test8
	* cache overflows to the storage
	* the clients tries to delete and get, shoulnd't be able to get it anymore
	* testing the storage delete functionality
	*/
	@Test
	public void testCacheOverflowDelete(){
		
		String[] keys = {"key0", "key1", "key2", "key3", "key4", "key5", "key6", "key7", "key8", "key9", "key10", "key11", "key12", "key13"};
		String[] values = {"value0", "value1", "value2", "value3", "value4", "value5", "value6", "value7", "value8", "value9", "value10", "value11", "value12", "value13"};
		
		KVMessage response = null;
		Exception ex = null;
		
		//client put 13 values in row
		
		for(int i=0; i<keys.length; i++){
			
			try {
				response = kvClient.put(unEscapeString(keys[i]), unEscapeString(values[i]));
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}
			
		}
		
		
		try {
			kvClient.put(unEscapeString(keys[0]), "null");//delete in the storage
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		
		//client 1 tries to get it back
		try {
			response = kvClient.get(keys[0]);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		

		assertTrue(ex == null && response.getStatus()==StatusType.GET_ERROR);
		
		
	}
	
	 /**
	 * test9
	 * client 1 and 2 try to get something concurrently
	 */
	@Test
	public void testConcurrentGet(){
		
		String key = "con_foo1";
		String value = "con_bar1";
		KVMessage response = null;
		KVMessage response2 = null;
		Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		try {
			response = kvClient.get(key);
			response2 = kvClient2.get(key);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		

		assertTrue(ex == null && response.getStatus() == StatusType.GET_SUCCESS && response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(response.getValue()));
		
		
	}
	
	 /**
	 * test10
	 * client 1 and 2 try to get something concurrently
	 * client 1 and 2 concurrently put something, they should get the same value back for the same key
	 */
	@Test
	public void testConcurrentPut(){
		
		String key = "con_foo2";
		String value1 = "con_bar2";
		String value2 = "con_bar3";
		KVMessage response = null;
		KVMessage response2 = null;
		Exception ex = null;

		try {
			response = kvClient.put(key, value1);
			response2 = kvClient2.put(key, value2);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		
		try {
			response = kvClient.get(key);
			response2 = kvClient2.get(key);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		

		assertTrue(ex == null && response.getStatus() == StatusType.GET_SUCCESS && response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(response.getValue()));
		
		
	}
	
	
	

	
}
