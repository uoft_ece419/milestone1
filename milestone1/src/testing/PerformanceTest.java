package testing;

import app_kvServer.KVServer;
import client.*;
import logger.LogSetup;

import java.util.UUID;

import org.apache.log4j.Level;

import java.io.IOException;
import java.lang.Math;
import java.util.Random;
import java.util.Timer;
import java.io.File.*;
import java.io.File;



/**
 * Used to measure the average latency for a given server setup and number of clients making requests.
 *
 */
public class PerformanceTest{
	
	private double[] average_serve_time_per_client;
    public static String storage_filename = "./server_storage.txt"; 

    
    
    
    
	/**
     * Used to store key value pairs in requests
     */
    private class KVPair
	{
	    private final String key;
	    private final String value;

	    public KVPair(String key, String value)
	    {
	        this.key   = key;
	        this.value = value;
	    }

	    public String key()   { return key; }
	    public String value() { return value; }
	}
	
	
	
	
	/**
	 * 
	 * Sets up the server, creates the client threads, creates the unique key-value array shared among clients.
	 * 
	 * @param num_clients: Number of client threads concurrently making put/get requests.
	 * @param cachesize: Server cache size (elements)
	 * @param unique_requests: Number of unique key value pairs, shared among all clients.
	 * @param number_of_requests: Number of random put, get requests from this array.
	 * @param strategy: LRU, LFU, FIFO
	 * @param port: Server listening port number
	 */
	public void performance_test(int num_clients, int cachesize, int unique_requests, int number_of_requests, String strategy, int port){
		
		
		// Before we start the server, 
		// clear the persistent storage:
		File persistent_storage = new File(storage_filename);
        if(persistent_storage.exists())
        	persistent_storage.delete();
		
        
		
		
		
		
		// Create the test server:
		//
		
		System.out.println("Creating the test server.");
		KVServer test_server = new KVServer(port, cachesize, strategy);
		test_server.setLevel("ERROR");
		test_server.start();
	
		try{
			Thread.sleep(2000); // Give the server time to get to accept() call
								// before clients spawn.
		}
		catch(Exception e){
			; 
		}
		
		
		
		
		// Create all the clients:
		System.out.println("Creating clients 0 through "+ (num_clients-1)+ ".");
		KVStore[] client_array = new KVStore[num_clients];
		average_serve_time_per_client = new double[num_clients];
		
		for(int i = 0; i < num_clients; i++){
			client_array[i] = new KVStore("localhost", port);
		}
		
		
		// Create all the unique key value pair requests
		//
		System.out.println("Creating the unique key value pairs.");
    	KVPair[] kvpair_array = new KVPair[unique_requests];
    	
    	for(int i = 0; i < unique_requests; i++){
    		
    		// Create random key and value pairs;
    		String key = UUID.randomUUID().toString().replaceAll("-", "");
    		key = key.substring(0, Math.min(19, key.length()));
    		String value = UUID.randomUUID().toString();
    		
    		kvpair_array[i] = new KVPair(key, value);
    	}
    	
    	
		
		
		// Start the client work for each client
		System.out.println("Running the client threads.");
    	Thread[] clients = new Thread[num_clients];
    	
		for(int i = 0; i < num_clients; i++){
			clients[i] = new Thread(new ClientThread(client_array[i], unique_requests, number_of_requests, kvpair_array, i));
		}
		for(int i = 0; i < num_clients; i++){
			clients[i].start();
		}
		for(int i = 0; i < num_clients; i++){
			try{
				clients[i].join();
			}
			catch(Exception e){
				System.out.println("Thread "+ i+ " failed.");
			}
		}
		
		// Disconnect the clients:
		for(int i = 0; i < num_clients; i++){
			client_array[i].disconnect();
		}
		
		// Close the server:
		test_server.stopServer();
		
		
		
		
		
		
		// Print the results:
		double running_total = 0.0;
		for(int i = 0; i < num_clients; i++){
			running_total += average_serve_time_per_client[i];
			//System.out.println("Average serve time per client " + i +": "+ average_serve_time_per_client[i]);
		}
		
		System.out.println("TEST PARAMETERS: #clients: " + num_clients + " cachesize: "+ cachesize + " unique pairs: "+unique_requests+
						   " requests/client: " + number_of_requests + " strategy: " + strategy);
		System.out.println("Average serve time averaged over all client requests: " + running_total/num_clients +"\n\n\n\n");
		
		
		try{
			Thread.sleep(2000); // Give the OS time to clean up resources.
		}
		catch(Exception e){
			; 
		}
	
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * The client worker thread that makes random requests from the request array.
	 *
	 */
	private class ClientThread implements Runnable{
	
		private int client_number;
	    private KVStore client_api;
	    private int num_requests, unique_requests;
	    private KVPair[] kv_requests;

	    public ClientThread(KVStore client_api, int unique_requests, int number_of_requests, KVPair[] array, int client_number) {
	    	this.unique_requests = unique_requests;
	        this.client_api = client_api;
	        this.num_requests = number_of_requests;
	        this.kv_requests = array;
	        this.client_number = client_number;
	    }

	    
	    
	    /**
	     * Client thread "work"
	     */
	    public void run() {
	    	
	    	double total_time = 0.0;
	    	long time_finish, time_start;
	    	double time_accumulated;
	    	
	    	try{
	    		client_api.connect();
	    	}
	    	catch(Exception e){
	    		System.out.println("Client "+ client_number + " failed to connect to the test server.");
	    		return;
	    	}
	    	if(!client_api.isRunning()){
	    		System.out.println("Client "+ client_number + " failed to connect to the test server.");
	    		return;
	    	}
	    	
	    	
	    	// Make the actual requests at random:
	    	//
	    	Random rng = new Random();

	    	for(int request_number = 0; request_number < num_requests; request_number++){
	    		
	    		// Form: random.nextInt((max-min)+1) + min;
	    	    int put_or_get = rng.nextInt(2);
	    	    int request_index = rng.nextInt(unique_requests);

		    	time_start = System.currentTimeMillis();
		    	
	    	    // PUT:
	    	    if(put_or_get == 1){
	    	    	try{
	    	    		client_api.put(kv_requests[request_index].key(), kv_requests[request_index].value());
	    	    	}
	    	    	catch(Exception e){
	    	    		System.out.println("Put request failed");
	    	    	}
	    	    }
	    	    
	    	    
	    	    
	    	    // GET:
	    	    else{
	    	    			    	    
	    	    	try{
	    	    		client_api.get(kv_requests[request_index].key());
	    	    	}
	    	    	catch(Exception e){
	    	    		System.out.println("Put request failed");
	    	    	}
	    	    }
	    	    
		    	time_finish = System.currentTimeMillis();
		    	time_accumulated = (time_finish-time_start)/1000.0;
		    	total_time += time_accumulated;
	    	}
	    
	    	
	    	// Write final result:
	    	average_serve_time_per_client[client_number] = total_time/num_requests;
	    }

	}
	
	
	
	

}
