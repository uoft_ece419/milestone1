package app_kvServer;


import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.File;
import java.io.IOException;

import logger.LogSetup;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.PriorityQueue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.net.SocketException;

public class KVServer extends Thread{
	
	private volatile Cache data_cache;

	Logger logger = Logger.getRootLogger();
	private ServerSocket serverSocket;
	private boolean running;
	
	// Parameters input into the command line:
	private int port;
    private int cachesize;
    private String strategy;  
    
    public static String storage_filename = "./server_storage.txt"; 
    
    private static ReadWriteLock storage_lock;
    
    
	/**
	 * Start KV Server at given port
	 * @param port given port for storage server to operate
	 * @param cacheSize specifies how many key-value pairs the server is allowed 
	 *           to keep in-memory
	 * @param strategy specifies the cache replacement strategy in case the cache 
	 *           is full and there is a GET- or PUT-request on a key that is 
	 *           currently not contained in the cache. Options are "FIFO", "LRU", 
	 *           and "LFU".
	 */
	public KVServer(int port, int cacheSize, String strategy) {
		this.port = port;
		this.cachesize = cacheSize;
		this.strategy = strategy;
	}
	
	/**
     * Initializes and starts the server. 
     * Loops until the the server should be closed.
     */
    public void run() {
    	
    	
		File f = new File(storage_filename);
		if(!f.exists() || f.isDirectory()) {//create file in current directory
		    
			logger.info("New Storage file Created\n");
			
			Path file = Paths.get(storage_filename);
			try {
			    Files.createFile(file);
			} catch (FileAlreadyExistsException x) {
			    System.err.format("file named %s" +
			        " already exists%n", file);
			} catch (IOException x) {
			    System.err.format("createFile error: %s%n", x);
			    logger.warn("createFile error: %s\n", x);
			}
			
			
		}else{
			logger.info("Using existing storage file" + f.getName());
		}

		//create persistent storage file lock
		//storage_lock = new ReadWriteLock[1];
		storage_lock = new ReentrantReadWriteLock();
    	
        
    	running = initializeServer();
        
        if(serverSocket != null) {
	        while(isRunning()){
	            try {
	            	
	            	
	            	// Its possible we're running but the socket is null:
	            	if(serverSocket != null){
	            		
	            		
	            		
	            		Socket client = null;
	            		try{
	            			client = serverSocket.accept();      
	            		}
	            		catch(SocketException e){
	            			logger.info("Closed the server in accept call. \n", e);
	            			// Do nothing, we only get here is the socket closes during the accept() call
	            			  // Only happens during performance testing calling serverClose()
	            		}
	            		
	            		
		                if(serverSocket != null && client != null){
			                data_cache = new Cache(cachesize,strategy);
		
			                ClientConnection connection = new ClientConnection(client, data_cache, storage_lock);
			                new Thread(connection).start();
			                
			                logger.info("Connected to " 
			                		+ client.getInetAddress().getHostName() 
			                		+  " on port " + client.getPort());
		                }
	            	}
	                
	            } catch (IOException e) {
	            	logger.error("Error! " +
	            			"Unable to establish connection. \n", e);
	            }
	        }
        }
        logger.info("Server stopped.");
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     * 
     * 
     * 
     * 
     * 
     *  Helper functions
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    
    private boolean isRunning() {
        return this.running;
    }

    /**
     * Stops the server insofar that it won't listen at the given port any more.
     */
    public void stopServer(){
        try {
			serverSocket.close();
			serverSocket = null;
			logger.info("Server terminated\n");
		} catch (IOException e) {
			logger.error("Error! " +
					"Unable to close socket on port: " + port, e);
		}
        running = false;
    }

    private boolean initializeServer() {
    	
    	logger.info("Initialize server: " + strategy + " cache size: " + cachesize);
    	try {
            serverSocket = new ServerSocket(port);
            logger.info("Server listening on port: " 
            		+ serverSocket.getLocalPort());    
            return true;
        
        } catch (IOException e) {
        	logger.error("Error! Cannot open server socket:");
            if(e instanceof BindException){
            	logger.error("Port " + port + " is already bound!");
            }
            return false;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
	
	
	/**
     * Main entry point for the echo server application. 
     * @param args contains the port number at args[0].
     * @param cacheSize specifies how many key-value pairs the server is allowed 
	 *           to keep in-memory (args[1])
	 * @param strategy specifies the cache replacement strategy in case the cache 
	 *           is full and there is a GET- or PUT-request on a key that is 
	 *           currently not contained in the cache. Options are "FIFO", "LRU", 
	 *           and "LFU". (args[2])
     */
    public static void main(String[] args) {
    	
    	try {
			new LogSetup("logs/server.log", Level.ALL);
			
			// Check number of arguments:
			if(args.length != 3) {
				System.out.println("Error! Invalid number of arguments!");
				System.out.println("Usage: Server <port> <cacheSize> <strategy>!");
			} 
			
			else {
			
				
				
				int port = Integer.parseInt(args[0]);
				int cacheSize = Integer.parseInt(args[1]);
				String strategy = args[2];
		
				
				//check cache size
				if(cacheSize<=0){
					
					System.out.println("Error! Invalid cache size!");
					System.out.println("Cache size has to be greater than 0");
					
				}
				
				// Check that strategy is a valid option
				//
				else if( !(strategy.equals("FIFO") || strategy.equals("LRU") || strategy.equals("LFU")) ){
					
					System.out.println("Error! Invalid strategy option!");
					System.out.println("Options: FIFO LRU LFU");
					
				}
				
				// Invalid strategy:
				else{
					new KVServer(port, cacheSize, strategy).start();
				}
				
				
				
			}
		} 
    	
    	// Catches log setup errors
    	catch (IOException e) {
			System.out.println("Error! Unable to initialize logger!");
			e.printStackTrace();
			System.exit(1);
		} 
    	// Catches invalid port number format
    	catch (NumberFormatException nfe) {
			System.out.println("Error! Invalid argument <port>! Not a number!");
			System.out.println("Usage: Server <port>!");
			System.exit(1);
		}
    }
    
    
    
    
    
    
    
    /**
     * 
     * Sets the level of the root logger to help filter out messages (useful for testing)
     * 
     * @param levelString: The desired logging level
     * 
     * @return: The new level
     */
    public String setLevel(String levelString) {
		
		if(levelString.equals(Level.ALL.toString())) {
			logger.setLevel(Level.ALL);
			return Level.ALL.toString();
		} else if(levelString.equals(Level.DEBUG.toString())) {
			logger.setLevel(Level.DEBUG);
			return Level.DEBUG.toString();
		} else if(levelString.equals(Level.INFO.toString())) {
			logger.setLevel(Level.INFO);
			return Level.INFO.toString();
		} else if(levelString.equals(Level.WARN.toString())) {
			logger.setLevel(Level.WARN);
			return Level.WARN.toString();
		} else if(levelString.equals(Level.ERROR.toString())) {
			logger.setLevel(Level.ERROR);
			return Level.ERROR.toString();
		} else if(levelString.equals(Level.FATAL.toString())) {
			logger.setLevel(Level.FATAL);
			return Level.FATAL.toString();
		} else if(levelString.equals(Level.OFF.toString())) {
			logger.setLevel(Level.OFF);
			return Level.OFF.toString();
		} else {
			return LogSetup.UNKNOWN_LEVEL;
		}
	}
  
}
